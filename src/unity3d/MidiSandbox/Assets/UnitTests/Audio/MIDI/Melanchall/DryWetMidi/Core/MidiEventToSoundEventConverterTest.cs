using Main.Audio;
using Main.Music;
using Melanchall.DryWetMidi.Common;
using Melanchall.DryWetMidi.Core;
using MidiSandbox.Audio;
using NUnit.Framework;
using static MidiSandbox.Audio.MIDI.Melanchall.DryWetMidi.Core.MidiEventToSoundEventConverter;

namespace UnitTests.Audio.MIDI.Melanchall.DryWetMidi.Core
{
    public class MidiEventToSoundEventConverterTest
    {
        [Test]
        public void C4()
        {
            NoteOnEvent inputNoteOnEvent = new NoteOnEvent(
                noteNumber: new SevenBitNumber(60),
                velocity: new SevenBitNumber(0)
            );
            SoundEvent expectedOutputSoundEvent = new SoundEvent(
                pitch: new Pitch(new Octave(4), new Note(1)),
                velocity: new Velocity(0)
            );

            SoundEvent actualOutputSoundEvent = ToSoundEvent(inputNoteOnEvent);

            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Pitch.Octave.Value,
                actual: actualOutputSoundEvent.Pitch.Octave.Value);
            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Pitch.Note.Value,
                actual: actualOutputSoundEvent.Pitch.Note.Value);
            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Velocity.Value,
                actual: actualOutputSoundEvent.Velocity.Value);
        }
        
        [Test]
        public void D4()
        {
            // MIDI Middle C (C4) = 60, so 62 = D
            NoteOnEvent inputNoteOnEvent = new NoteOnEvent(
                noteNumber: new SevenBitNumber(62),
                velocity: new SevenBitNumber(90)
            );
            // 1 = C, 2 = C#/Db, 3 = D
            SoundEvent expectedOutputSoundEvent = new SoundEvent(
                pitch: new Pitch(new Octave(4), new Note(3)),
                velocity: new Velocity(90 / 127.0)
            );

            SoundEvent actualOutputSoundEvent = ToSoundEvent(inputNoteOnEvent);

            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Pitch.Octave.Value,
                actual: actualOutputSoundEvent.Pitch.Octave.Value);
            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Pitch.Note.Value,
                actual: actualOutputSoundEvent.Pitch.Note.Value);
            Assert.AreEqual(
                expected: expectedOutputSoundEvent.Velocity.Value,
                actual: actualOutputSoundEvent.Velocity.Value);
        }
    }
}
