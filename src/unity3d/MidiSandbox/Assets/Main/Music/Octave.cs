using System;

namespace Main.Music
{
    // Octave as defined in European music theory
    public class Octave
    {
        public Int32 Value { get; }

        public Octave(Int32 value) { Value = value; }
    }
}
