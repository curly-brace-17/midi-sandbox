using System;

namespace Main.Music
{
    // Note in pitch class understanding, numerical representation of a pitch in its octave, as defined by European
    // music theory: C = 1, C#/Db = 2, ... 
    public class Note
    {
        public Int32 Value { get; }
        
        public Note(Int32 value) { Value = value; }
    }
}
