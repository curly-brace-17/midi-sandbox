namespace Main.Music
{
    public class Pitch
    {
        public Octave Octave { get; }

        public Note Note { get; }

        public Pitch(Octave octave, Note note)
        {
            Octave = octave;
            Note = note;
        }
    }
}
