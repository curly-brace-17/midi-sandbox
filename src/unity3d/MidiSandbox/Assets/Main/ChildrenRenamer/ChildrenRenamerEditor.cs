using UnityEditor;
using UnityEngine;

namespace Main.ChildrenRenamer
{
  [CustomEditor(typeof(ChildrenRenamer))]
  public class ChildrenRenamerEditor : Editor
  {
    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      ChildrenRenamer childrenRenamer = (ChildrenRenamer) target;
      if (GUILayout.Button("Rename Children"))
      {
        childrenRenamer.RenameChildren();
      }
    }
  }
}
