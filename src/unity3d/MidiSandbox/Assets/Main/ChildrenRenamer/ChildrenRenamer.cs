using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Main.ChildrenRenamer
{
    public class ChildrenRenamer : MonoBehaviour
    {
        public String matchPattern;
        public String replacementPattern;

        public Transform root;

        public void RenameChildren()
        {
            foreach (Transform child in root.transform)
            {
                child.name = Regex.Replace(child.name, matchPattern, replacementPattern);
            }
        }
    }
}
