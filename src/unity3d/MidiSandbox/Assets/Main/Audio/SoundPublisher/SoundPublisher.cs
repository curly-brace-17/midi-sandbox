using System;
using System.IO;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Devices;
using MidiSandbox.Audio;
using UnityEngine;
using static Main.Audio.MIDI.Melanchall.DryWetMidi.Devices.Constants;
using static MidiSandbox.Audio.MIDI.Melanchall.DryWetMidi.Core.MidiEventToSoundEventConverter;
using Object = System.Object;

namespace Main.Audio.SoundPublisher
{
    public class SoundPublisher : MonoBehaviour
    {
        public event EventHandler<SoundEvent> OnNote;


        // Events source - MIDI file name without extension (assumed ".mid") used to publish events.
        [SerializeField] private String eventsSourceFileName;

        // Audio synchronized with events source. Can be a different, more richer version.
        [SerializeField] private AudioSource audioSource;


        private MidiFile midiFile;

        private Playback playback;


        private void Start()
        {
            midiFile = MidiFile.Read(GetFullPath());
            
            playback = midiFile.GetPlayback(ManualPlaybackSettings);

            playback.EventPlayed += (Object sender, MidiEventPlayedEventArgs args) =>
            {
                MidiEvent midiEvent = args.Event;
                if (midiEvent.EventType == MidiEventType.NoteOn)
                {
                    NoteOnEvent noteOnEvent = (NoteOnEvent) midiEvent;
                    SoundEvent soundEvent = ToSoundEvent(noteOnEvent);
                    OnNote?.Invoke(this, soundEvent);
                }
            };

            if (audioSource) audioSource.Play();
            playback.Start();

            playback.Finished += (sender, args) => { playback.Dispose(); };
        }

        private void Update()
        {
            if (playback.IsRunning) playback.TickClock();
        }

        private String GetFullPath()
        {
            return Path.Combine(Application.streamingAssetsPath, $"{eventsSourceFileName}.mid");
        }
    }
}
