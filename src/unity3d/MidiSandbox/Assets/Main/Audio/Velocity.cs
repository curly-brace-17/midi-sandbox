using System;

namespace Main.Audio
{
    public class Velocity
    {
        public Double Value { get; }
        
        public Velocity(Double value) { Value = value; }
    }
}
