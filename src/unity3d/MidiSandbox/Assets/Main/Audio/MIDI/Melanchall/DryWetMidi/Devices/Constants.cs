using Melanchall.DryWetMidi.Devices;

namespace Main.Audio.MIDI.Melanchall.DryWetMidi.Devices
{
    public static class Constants
    {
        // Playback settings used for manual ticking.
        // https://melanchall.github.io/drywetmidi/articles/playback/Tick-generator.html#manual-ticking
        public static PlaybackSettings ManualPlaybackSettings =
            new PlaybackSettings
            {
                ClockSettings = new MidiClockSettings {CreateTickGeneratorCallback = () => null}
            };
    }
}
