using System;
using Main.Audio;
using Main.Music;
using Melanchall.DryWetMidi.Core;

namespace MidiSandbox.Audio.MIDI.Melanchall.DryWetMidi.Core
{
    public class MidiEventToSoundEventConverter
    {
        public static SoundEvent ToSoundEvent(NoteOnEvent noteOnEvent)
        {
            Int32 octave = Math.DivRem(noteOnEvent.NoteNumber, 12, out Int32 note);
            return new SoundEvent(
                pitch: new Pitch(
                    octave: new Octave(-1 + octave),
                    note: new Note(1 + note)),
                velocity: new Velocity(noteOnEvent.Velocity / 127.0)
            );
        }
    }
}
