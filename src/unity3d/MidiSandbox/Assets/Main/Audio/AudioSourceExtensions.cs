using System;
using UnityEngine;

namespace Main.Audio
{
  public class AudioSourceExtensions
  {
    // The most accurate way to get an AudioSource time: https://medium.com/@thibautdumont/rhythm-game-with-unity3d-achieve-latency-free-sync-android-and-other-platforms-c05fa8e2718b
    public static Double GetAudioTime(Double startTime)
    {
      return AudioSettings.dspTime - startTime;
    }

    // Play AudioSource and return the exact time when it started to play. It schedules the start for one millisecond.
    // Explanation in https://medium.com/@thibautdumont/rhythm-game-with-unity3d-achieve-latency-free-sync-android-and-other-platforms-c05fa8e2718b
    public static Double Play(AudioSource audioSource)
    {
      var startTime = AudioSettings.dspTime + 1;
      audioSource.PlayScheduled(startTime);
      return startTime;
    }
  }
}
