using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Interaction;

namespace Main.Audio
{
  public class MidiEventsExtractor
  {
    public static List<Double> ExtractTimestamps(Stream midiFileStream)
    {
      return ExtractTimestamps(MidiFile.Read(midiFileStream));
    }
    
    public static List<Double> ExtractTimestamps(String midiFileName)
    {
      return ExtractTimestamps(MidiFile.Read(midiFileName));
    }

    public static List<Double> ExtractTimestamps(MidiFile midiFile)
    {
      List<Note> notes = midiFile.GetNotes().OrderBy(note => note.Time).ToList();
      List<Double> notesTimestamps = notes.Select(note =>
        ConvertMetricTimeSpanToSeconds(GetMetricTimeSpan(note, midiFile))).ToList();
      return notesTimestamps;
    }

    private static Double ConvertMetricTimeSpanToSeconds(MetricTimeSpan metricTimeSpan)
    {
      return (Double) metricTimeSpan.Minutes * 60f +
             metricTimeSpan.Seconds +
             (Double) metricTimeSpan.Milliseconds / 1000f;
    }

    private static MetricTimeSpan GetMetricTimeSpan(ITimedObject note, MidiFile midiFile)
    {
      return TimeConverter.ConvertTo<MetricTimeSpan>(note.Time, midiFile.GetTempoMap());
    }
  }
}
