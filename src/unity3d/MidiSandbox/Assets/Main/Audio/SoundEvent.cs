using Main.Audio;
using Main.Music;

namespace MidiSandbox.Audio
{
    public class SoundEvent
    {
        public Pitch Pitch { get; }

        public Velocity Velocity { get; }

        public SoundEvent(Pitch pitch, Velocity velocity)
        {
            Pitch = pitch;
            Velocity = velocity;
        }
    }
}
