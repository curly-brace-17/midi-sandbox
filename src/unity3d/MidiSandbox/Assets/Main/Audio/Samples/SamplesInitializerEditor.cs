using UnityEditor;
using UnityEngine;

namespace Main.Audio.Samples
{
  [CustomEditor(typeof(SamplesInitializer))]
  public class SamplesInitializerEditor : Editor
  {
    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      SamplesInitializer samplesInitializer = (SamplesInitializer) target;
      if (GUILayout.Button("Initialize Samples")) samplesInitializer.InitializeSamples();
    }
  }
}
