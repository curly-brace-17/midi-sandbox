using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Main.Audio.Samples
{
  [ExecuteInEditMode]
  public class SamplesInitializer : MonoBehaviour
  {
    [SerializeField]
    private GameObject sampleAudioSource;

    public void InitializeSamples()
    {
      Debug.Log("Initializing samples");
      foreach (Transform child in transform) Destroy(child);
      Debug.Log("Deleted children");

      Debug.Log("Loading samples");
      List<AudioClip> audioClips = Resources.LoadAll<AudioClip>("Audio/Samples/MathematicaGuitar").ToList();
      Debug.Log("Loaded samples");

      Debug.Log("Instantiating GameObjects");
      foreach (AudioClip audioClip in audioClips)
      {
        GameObject gameObject = (GameObject) PrefabUtility.InstantiatePrefab(sampleAudioSource);
        gameObject.transform.SetParent(transform, worldPositionStays: false);
        gameObject.name = $"AudioSource_{audioClip.name}";
        AudioSource audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.clip = audioClip;
      }
      Debug.Log("Instantiated GameObjects");
    }
  }
}
