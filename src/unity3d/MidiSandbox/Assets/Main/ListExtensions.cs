using System;
using System.Collections.Generic;

namespace Main
{
  public static class ListExtensions
  {
    public static T First<T>(this List<T> list) { return list[0]; }
    public static T Last<T>(this List<T> list) { return list[list.Count - 1]; }
    
    public static void OrderlyAdd<T>(this List<T> list, T item, Func<T, T, Int32> comparer)
    {
      Int32 Compare(T a, T b) { return comparer.Invoke(a, b); }

      if (list.Count == 0) { list.Add(item); }
      else if (Compare(list.Last(),item) <= 0) { list.Add(item); }
      else if (Compare(list.First(), item) >= 0) { list.Insert(0, item); }
      else {
        Int32 index = list.BinarySearch(item);
        if (index < 0)
          index = ~index;
        list.Insert(index, item);
      }
    }

    public static void OrderlyAdd<T>(this List<T> list, T item)
      where T : IComparable<T>
    {
      list.OrderlyAdd(item, (a, b) => a.CompareTo(b));
    }
  }
}
