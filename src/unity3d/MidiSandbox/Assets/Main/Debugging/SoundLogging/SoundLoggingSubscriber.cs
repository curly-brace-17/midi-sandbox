using Main.Audio.SoundPublisher;
using MidiSandbox.Audio;
using Newtonsoft.Json;
using UnityEngine;
using Object = System.Object;

namespace Main.Debugging
{
    public class SoundLoggingSubscriber : MonoBehaviour
    {
        [SerializeField]
        private SoundPublisher soundPublisher;

        private void Start()
        {
            soundPublisher.OnNote += Log;
        }

        private void Log(Object sender, SoundEvent e)
        {
            Debug.Log(JsonConvert.SerializeObject(e));
        }
    }
}
