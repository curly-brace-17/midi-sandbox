using System;

namespace Main.Time
{
    public class Minute
    {
        public Double Value { get; }
        
        public Minute(Double value) { Value = value; }
    }
}
