using System;

namespace Main.Time
{
    public class Second
    {
        public Double Value { get; }
        
        public Second(Double value) { Value = value; }
    }
}
