using System;

namespace Main.Time
{
    public class Hour
    {
        public Double Value { get; }

        public Hour(Double value) { Value = value; }
    }
}
