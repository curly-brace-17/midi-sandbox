﻿using System;
using System.IO;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Tools;
using NUnit.Framework;

namespace DanOak.MidiSandbox.Melanchall.DryWetMidi.Csv
{
    [TestFixture]
    public class CsvTests
    {
        public const String RESOURCES = "Resources";
        public const String OUTPUT = "Output";
        
        private CsvConverter csvConverter = new CsvConverter();

        [Test]
        public void SingleNoteMidi()
        {
            csvConverter.ConvertMidiFileToCsv(
                MidiFile.Read(Path.Combine(RESOURCES, "single_middle_c.mid")),
                filePath: Path.Combine(OUTPUT, "single_middle_c.csv"),
                overwriteFile: true
            );
        }
        
        [Test]
        public void NokiaGranValsTune()
        {
            csvConverter.ConvertMidiFileToCsv(
                MidiFile.Read(Path.Combine(RESOURCES, "nokia_gran_vals_tune.mid")),
                filePath: Path.Combine(OUTPUT, "nokia_gran_vals_tune.csv"),
                overwriteFile: true
            );
        }
        
        [Test]
        public void TarregaRosita()
        {
            csvConverter.ConvertMidiFileToCsv(
                MidiFile.Read(Path.Combine(RESOURCES, "Tarrega_Rosita.mid")),
                filePath: Path.Combine(OUTPUT, "TarregaRosita.csv"),
                overwriteFile: true
            );
        }
    }
}
